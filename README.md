### react-notes

## React Install:
```
npx create-react-app my-app
cd my-app
npm start
44-0 Milestone overview
```

MODERN FRONT-END CORE CONCEPTS
```
1.	Web component
2.	Dynamic create HTML elements
3.	Single Page Application(SPA)
4.	Routing, Route parameter
5.	Destructuring and props
6.	Website state
7.	Folder structure
```
React Core Concept:
```
1.	Core concepts Discussion
2.	Working with JSX, Dynamic content, Dynamic Style
3.	Pass Dynamic data to components, props
4.	Create multiple components
5.	State, component state hook and set state
6.	Load dynamic data , API call useEffect integrate state
```
HOW REACT WORKS
```
1.	What is JSX, React Component, babel transpiler
2.	Components and how props works
3.	Unidirectional data flow
4.	How states works, asynchronous
5.	How React Hook works
6.	Props vs state
7.	Properties vs attributes
8.	What Is React, when to use it
9.	Render, virtual dom, diff algorithm
10.	React component lifecycle
```

YOU WILL ALSO LEARN
```
1.	ES6 Modules
2.	Import, Expart, Default
3.	Relative Path
4.	Create and Load Fake data
5.	Array reduce
6.	Local storage functionality
7.	Good UI vs bad UI
8.	Themeforestm, freepik, flaticon, unsplash
9.	Add or remove key to an object
10.	Wrap event handler with arrow function
11.	Local stortage with more functionalites
```
